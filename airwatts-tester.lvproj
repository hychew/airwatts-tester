﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Controls" Type="Folder">
			<Item Name="ConsumerMessage.ctl" Type="VI" URL="../ConsumerMessage.ctl"/>
		</Item>
		<Item Name="MT210 Averaging Function.vi" Type="VI" URL="../MT210 Averaging Function.vi"/>
		<Item Name="MT210 Get Reading.vi" Type="VI" URL="../MT210 Get Reading.vi"/>
		<Item Name="MT210 Open.vi" Type="VI" URL="../MT210 Open.vi"/>
		<Item Name="read-pressure.vi" Type="VI" URL="../read-pressure.vi"/>
		<Item Name="Write Read MT210.vi" Type="VI" URL="../Write Read MT210.vi"/>
		<Item Name="Write to MT210.vi" Type="VI" URL="../Write to MT210.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="YKMT110 Read Measurement.vi" Type="VI" URL="/&lt;instrlib&gt;/YKMT110/ykmt110.llb/YKMT110 Read Measurement.vi"/>
				<Item Name="YKMT110 Utility Analyze Data.vi" Type="VI" URL="/&lt;instrlib&gt;/YKMT110/ykmt110u.llb/YKMT110 Utility Analyze Data.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
